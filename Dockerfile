# Temporary stages ===========
FROM ubuntu:20.04 as gtest-build

ARG GTEST_VERSION=1.10.0

ENV DEBIAN_FRONTEND=noninteractive
testing
RUN apt-get update \
    && apt-get install -y --no-install-recommends cmake build-essential \
    && rm -rf /var/lib/apt/lists/*

ADD https://github.com/google/googletest/archive/release-${GTEST_VERSION}.tar.gz /
RUN tar -xzf /release-*.tar.gz

RUN cd /googletest-release-${GTEST_VERSION}/ \
    && mkdir build
WORKDIR /googletest-release-${GTEST_VERSION}/build/

RUN cmake ../
RUN make -j

# Main image =================
FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

# First installation step from:
# https://code.visualstudio.com/docs/remote/containers-advanced#_reducing-dockerfile-build-warnings

RUN apt-get update \
    && apt-get install -y gdb \
    # vscode-recommended tools
    && apt-get install -y --no-install-recommends apt-utils dialog 2>&1 \
    # misc convenience tools; wget for toolchain download below
    && apt-get install -y --no-install-recommends git wget curl vim gdb clang-format-10 valgrind bash-completion \
    # Python and scons
    && apt-get install -y --no-install-recommends python3 python3-pip scons \
    # documentation generation
    && apt-get install -y --no-install-recommends doxygen \
    # compiler and libs for building tests targeting Linux
    && apt-get install -y build-essential libboost-all-dev gcc-10 g++-10 lcov gcovr \
    && rm -rf /var/lib/apt/lists/*

# Update gcc and g++ symbolic links to point to version 10
RUN update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 90 --slave /usr/bin/g++ g++ /usr/bin/g++-10 --slave /usr/bin/gcov gcov /usr/bin/gcov-10
RUN pip3 install lbuild pyelftools modm

# "install" the gcc ARM cross-compiler
RUN wget -q https://developer.arm.com/-/media/Files/downloads/gnu-rm/10-2020q4/gcc-arm-none-eabi-10-2020-q4-major-x86_64-linux.tar.bz2 -O gcc-arm.tar.bz2 \
    && tar -xjf /gcc-arm.tar.bz2 \
    && rm gcc-arm.tar.bz2
ENV PATH="/gcc-arm-none-eabi-10-2020-q4-major/bin:$PATH"

ARG GTEST_VERSION=1.10.0
# "install" gtest and gmock
COPY --from=gtest-build /googletest-release-${GTEST_VERSION}/googletest/include/ /usr/local/include/
COPY --from=gtest-build /googletest-release-${GTEST_VERSION}/googlemock/include/ /usr/local/include/
COPY --from=gtest-build /googletest-release-${GTEST_VERSION}/build/lib/libg*.a /usr/local/lib/

ARG USERNAME=aruw
ARG USER_UID=1000
ARG USER_GID=$USER_UID

# Create a non-root default user
# From: https://code.visualstudio.com/docs/remote/containers-advanced#_adding-a-nonroot-user-to-your-dev-container
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME -s /bin/bash \
    && apt-get update \
    && apt-get install -y sudo \
    && rm -rf /var/lib/apt/lists/* \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

USER $USERNAME

# ensure that aruw owns .vscode-server dir. Otherwise, mounting the extensions
# volume will create the directory as root, causing vscode to fail when loading.
RUN mkdir -p /home/aruw/.vscode-server/extensions/

ENV DEBIAN_FRONTEND=dialog
